# com.sixt.gradle.quality-gate

## Usage

Edit Your Gradle build file (usually `build.gradle`) with following

* Be sure that you added maven repositories `https://plugins.gradle.org/m2`
* Update `buildscript` section with `classpath 'com.sixt.gradle:quality-gate:<version>'`
* Apply the plugin as this - `apply plugin: 'com.sixt.gradle.quality-gate'`

### Example

Example `build.gradle`

```
buildscript {
    ext {
        ...
    }

    repositories {
        ...
        maven {
                    url {"https://plugins.gradle.org/m2/"}
              }
    }

    dependencies {
        ...
         classpath 'com.sixt.gradle:quality-gate:1.1.0'
    }
}

apply plugin: 'java'
apply plugin: 'groovy'
...
apply plugin: 'com.sixt.gradle.quality-gate'
```

**! Make sure you add `config/generated` to `.gitignore` so generated files won't be committed on accident.**

## Technical Details
Plugin applies `checkstyle`, `spotbugs` plugins and sets pre-defined IntelliJ Idea code style formatter settings.

Also package circular dependency check will be performed and break the build if violations are found.

Main sourceset will get these plugins applied and pre-defined configs will be places in the following locations:
- TBD checkstyle config
- TBD spotbug config

Modifying formatter settings in Idea won't have lasting effect as they will be overwritten by the plugin on next gradle refresh.

## Q&A
### How to disable build breaking for some checks?
Just set `ignoreFailures` property for all task of some plugin in gradle. Like this:
```
tasks.withType(org.gradle.api.plugins.quality.Checkstyle) {
    ignoreFailures = true
}
```

or

```
tasks.withType(com.github.spotbugs.SpotBugsTask) {
    ignoreFailures = true
}
```
### How to disable some tasks completely?
Same as ignoring failures, but just disable the task like this:
```
circularDependencyCheck.enabled = false
```
```
tasks.withType(org.gradle.api.plugins.quality.Checkstyle) {
    enabled = false
}
```
or
```
tasks.withType(com.github.spotbugs.SpotBugsTask) {
    enabled = false
}
```
### How to make exclusions for some rules?
#### Spotbugs
1) Use `@SuppressFBWarnings` annotation to suppress rules in place. This is a preferred way to do it.
2) Create a `config/spotbugs/exclude.xml` file similar to generated one and add your exclusion there. This file will get merged with the generated one.
#### Checkstyle
1) Use `@SuppressWarnings` annotation to suppress rules in place. This is a preferred way to do it.
2) Create a `config/checkstyle/suppressions.xml` and suppress some rules like in [this example](https://github.com/checkstyle/checkstyle/blob/master/config/suppressions.xml). 