package com.sixt.gradle.codequality;

import org.gradle.api.Task;

interface TaskUtils {

    static void disableTaskWithGenInName(Task task) {
        if (task.getName().contains("Gen")) {
            task.setEnabled(false);
        }
    }

}
