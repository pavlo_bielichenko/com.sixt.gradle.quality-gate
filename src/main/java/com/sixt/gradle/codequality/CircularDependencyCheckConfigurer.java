package com.sixt.gradle.codequality;

import java.util.StringJoiner;

import org.gradle.api.GradleException;
import org.gradle.api.Project;

import guru.nidi.codeassert.config.AnalyzerConfig;
import guru.nidi.codeassert.dependency.DependencyAnalyzer;
import guru.nidi.codeassert.dependency.DependencyResult;

class CircularDependencyCheckConfigurer implements Runnable {

    private static final String CIRCULAR_DEPENDENCY_CHECK_TASK_NAME = "circularDependencyCheck";

    private final Project project;

    CircularDependencyCheckConfigurer(Project project) {
        this.project = project;
    }

    @Override
    public void run() {
        //noinspection UnstableApiUsage
        project.task(CIRCULAR_DEPENDENCY_CHECK_TASK_NAME, task -> {
            task.setGroup("verification");
            task.doFirst(action -> circularDependencyScan());
            task.dependsOn("assemble");
        });
        project.getTasks().getByName("assemble").finalizedBy(CIRCULAR_DEPENDENCY_CHECK_TASK_NAME);
    }

    private void circularDependencyScan() {
        AnalyzerConfig config = AnalyzerConfig.gradle().mainAndTest();

        DependencyResult dependencyResult = new DependencyAnalyzer(config).analyze();

        int circularDependencies = dependencyResult.findings().getCycles().size();
        if (circularDependencies > 0) {
            StringJoiner joiner = new StringJoiner("\n");
            joiner.add("Circular dependencies of packages found");
            joiner.add("List of cycles:");

            joiner.add(dependencyResult
                .findings()
                .getCycles()
                .toString()
                .replaceAll("=", ":")
                .replaceAll("^\\[\\{", "[{\n\t")
                .replaceAll("}]$", "\n}]")
                .replaceAll(":\\{", ": {\n\t\t")
                .replaceAll("], ", "]\n\t\t")
                .replaceAll("]}, ", "]\n\t}\n\t")
                .replaceAll("]}\n", "]\n\t}\n")
                .replaceAll("}}, \\{", "\n\t}\n}, {\n\t")
                .replaceAll("\n\\{, ", "\n{\n"));
            throw new GradleException(joiner.toString());
        }
    }
}
