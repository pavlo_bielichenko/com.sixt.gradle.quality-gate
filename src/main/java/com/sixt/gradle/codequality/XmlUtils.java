package com.sixt.gradle.codequality;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.gradle.api.GradleException;
import org.xml.sax.SAXException;

import groovy.util.XmlSlurper;
import groovy.util.slurpersupport.GPathResult;
import groovy.util.slurpersupport.Node;
import groovy.xml.XmlUtil;

interface XmlUtils {

    @SuppressWarnings("unchecked")
    static String mergeXmls(String first, String second) {
        try {
            XmlSlurper xmlSlurper = new XmlSlurper(false, false);
            GPathResult firstXml = xmlSlurper.parseText(first);
            GPathResult secondXml = xmlSlurper.parseText(second);
            ((Node) firstXml.getAt(0)).children().add(secondXml.children());
            return XmlUtil.serialize(firstXml);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new GradleException("Unable to combine xmls", e);
        }
    }

}
