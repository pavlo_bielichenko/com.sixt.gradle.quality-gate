package com.sixt.gradle.codequality;

import static com.sixt.gradle.codequality.ToolsVersionLoader.getSpotbugsToolVersion;
import static java.util.Collections.singletonList;

import java.io.File;
import java.nio.file.Path;

import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.SourceSet;

import com.github.spotbugs.SpotBugsExtension;
import com.github.spotbugs.SpotBugsTask;

class SpotbugsConfigurer implements Runnable {

    private static final String SPOTBUGS_PLUGIN_NAME = "com.github.spotbugs";

    private static final String MAIN_SOURCESET_NAME = "main";

    private final Project project;

    SpotbugsConfigurer(Project project) {
        this.project = project;
    }

    @Override
    public void run() {
        project.getPluginManager().apply(SPOTBUGS_PLUGIN_NAME);

        SourceSet mainSourceSet = project.getConvention()
            .getPlugin(JavaPluginConvention.class)
            .getSourceSets()
            .getByName(MAIN_SOURCESET_NAME);

        project.getDependencies()
            .add(mainSourceSet.getCompileOnlyConfigurationName(),
                "com.github.spotbugs:spotbugs-annotations:" + getSpotbugsToolVersion());

        project.getExtensions().configure(SpotBugsExtension.class, spotBugsExtension -> {
            spotBugsExtension.setToolVersion(getSpotbugsToolVersion());

            // breaks on spock tests and generated sources
            spotBugsExtension.setSourceSets(singletonList(mainSourceSet));

            spotBugsExtension.setExcludeFilter(prepareConfigurationFile());

            spotBugsExtension.setEffort("max");
            spotBugsExtension.setReportLevel("low");
        });

        project.getTasks().withType(SpotBugsTask.class, task -> {
            if (!"spotbugsMain".equals(task.getName())) {
                task.setEnabled(false);
                return;
            }
            //noinspection UnstableApiUsage
            task.setClasspath(task.getClasspath().plus(mainSourceSet.getAnnotationProcessorPath()));

            task.reports(reports -> {
                // only 1 can be active at time - https://github.com/spotbugs/spotbugs-gradle-plugin/issues/114
                boolean isHtml = project.findProperty("spotbugsHtml") != null;
                //noinspection UnstableApiUsage
                reports.getHtml().setEnabled(isHtml);
                //noinspection UnstableApiUsage
                reports.getXml().setEnabled(!isHtml);
            });
        });
    }

    private File prepareConfigurationFile() {
        File projectSpecificExcludes = ResourceUtils.findFileInDir(project.getRootDir(), "config/spotbugs/exclude.xml").toFile();

        String globalExclude = ResourceUtils.readResourceFromClasspath("spotbugs/exclude.xml");
        String excludeXml = projectSpecificExcludes.exists()
            ? XmlUtils.mergeXmls(ResourceUtils.readFile(projectSpecificExcludes), globalExclude)
            : globalExclude;

        Path targetExcludeFile = ResourceUtils.findFileInDir(project.getRootDir(), "config/generated/spotbugs/exclude.xml");
        ResourceUtils.writeTextToFile(excludeXml, targetExcludeFile);
        return targetExcludeFile.toFile();
    }
}
