package com.sixt.gradle.codequality;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Collections.emptyMap;
import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.function.Consumer;

import javax.annotation.Nonnull;

import org.gradle.api.Action;
import org.gradle.api.GradleException;
import org.gradle.api.Project;

class IdeaCodeStyleConfigurer implements Runnable {

    private static final String IDEA_PLUGIN_NAME = "idea";

    private static final String CODE_STYLES = "codeStyles";

    private final Project project;

    private final Path ideaCodeStylesPath;

    IdeaCodeStyleConfigurer(Project project) {
        this.project = project;
        this.ideaCodeStylesPath = Paths.get(project.getRootDir().getPath(), ".idea", CODE_STYLES);
    }

    @Override
    public void run() {
        project.getPluginManager().apply(IDEA_PLUGIN_NAME);
        project.afterEvaluate(copyIdeaFiles());
    }

    private ThrowingConsumer<Project> copyIdeaFiles() {
        return evaluatedProject -> {
            Files.createDirectories(ideaCodeStylesPath);
            URL codeStylesResource = requireNonNull(this.getClass().getClassLoader().getResource(CODE_STYLES));
            try (FileSystem fs = FileSystems.newFileSystem(codeStylesResource.toURI(), emptyMap())) {
                Files.walk(fs.getPath(CODE_STYLES))
                    .filter(Files::isRegularFile)
                    .forEach(copyFileToIdeaSettings());
            }
        };
    }

    private ThrowingConsumer<Path> copyFileToIdeaSettings() {
        return file -> Files.copy(
            file,
            ideaCodeStylesPath.resolve(Objects.toString(file.getFileName())),
            REPLACE_EXISTING);
    }

    private interface ThrowingConsumer<T> extends Consumer<T>, Action<T> {

        @Override
        default void accept(T t) {
            try {
                this.acceptThrowing(t);
            } catch (Exception e) {
                throw new GradleException("Can't copy Idea code style files", e);
            }
        }

        @Override
        default void execute(@Nonnull T t) {
            this.accept(t);
        }

        void acceptThrowing(T t) throws IOException, URISyntaxException;
    }
}
