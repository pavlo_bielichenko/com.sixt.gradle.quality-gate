package com.sixt.gradle.codequality;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

import java.io.File;
import java.nio.file.Path;

import org.gradle.api.Project;
import org.gradle.api.plugins.quality.Checkstyle;
import org.gradle.api.plugins.quality.CheckstyleExtension;

class CheckstyleConfigurer implements Runnable {

    private static final String CHECKSTYLE_PLUGIN_NAME = "checkstyle";

    private final Project project;

    CheckstyleConfigurer(Project project) {
        this.project = project;
    }

    @Override
    public void run() {
        project.getPluginManager().apply(CHECKSTYLE_PLUGIN_NAME);

        project.getExtensions().configure(CheckstyleExtension.class, checkstyle -> {
            checkstyle.setToolVersion(ToolsVersionLoader.getCheckstyleToolVersion());

            checkstyle.setConfigFile(prepareConfigurationFile());
        });

        project.getTasks().withType(Checkstyle.class, task -> {
            TaskUtils.disableTaskWithGenInName(task);

            requireNonNull(task.getConfigProperties()).put("checkstyle_config_dir",
                format("%s/config/checkstyle", project.getRootDir().getPath()));

            task.reports(reports -> {
                //noinspection UnstableApiUsage
                reports.getHtml().setEnabled(true);
                //noinspection UnstableApiUsage
                reports.getXml().setEnabled(true);
            });
        });
    }

    private File prepareConfigurationFile() {
        Path targetCheckstyleFile = ResourceUtils.findFileInDir(project.getRootDir(), "config/generated/checkstyle/checkstyle.xml");
        ResourceUtils.writeTextToFile(ResourceUtils.readResourceFromClasspath("checkstyle/checkstyle.xml"), targetCheckstyleFile);
        return targetCheckstyleFile.toFile();
    }
}
