package com.sixt.gradle.codequality;

import static java.lang.String.format;
import static java.lang.System.lineSeparator;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import org.gradle.api.GradleException;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

interface ResourceUtils {

    static String readResourceFromClasspath(String resourcePath) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(requireNonNull(ResourceUtils.class
            .getClassLoader()
            .getResourceAsStream(resourcePath)), UTF_8))) {
            return br.lines().collect(joining(lineSeparator()));
        } catch (IOException e) {
            throw new GradleException(format("Can't read resource %s", resourcePath), e);
        }
    }

    static Path findFileInDir(File root, String filePath) {
        return root.toPath().resolve(filePath);
    }

    @SuppressFBWarnings("NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE")
    static void writeTextToFile(String text, Path path) {
        try {
            Files.createDirectories(path.getParent());
            try (BufferedWriter writer = Files.newBufferedWriter(path, UTF_8)) {
                writer.write(text);
            }
        } catch (IOException e) {
            throw new GradleException(format("Can't write to file %s", path.getFileName()), e);
        }
    }

    static String readFile(File file) {
        try {
            return Files.readAllLines(file.toPath()).stream().collect(joining(lineSeparator()));
        } catch (IOException e) {
            throw new GradleException(format("Can't read file %s", file.toPath()), e);
        }
    }

}
