package com.sixt.gradle.codequality;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gradle.api.GradleException;

final class ToolsVersionLoader {

    private static final String SPOTBUGS_ARTIFACT_NAME = "spotbugs";

    private static final String CHECKSTYLE_ARTIFACT_NAME = "checkstyle";

    private static final Properties VERSION_PROPERTIES = new Properties();

    static {
        try (InputStream propsStream = ToolsVersionLoader.class.getClassLoader()
            .getResourceAsStream("code-quality-versions.properties")) {
            VERSION_PROPERTIES.load(propsStream);
        } catch (IOException e) {
            throw new GradleException("Couldn't load code-quality-versions.properties file from code-quality plugin", e);
        }
    }

    private ToolsVersionLoader() {
    }

    static String getCheckstyleToolVersion() {
        return getToolVersion(CHECKSTYLE_ARTIFACT_NAME);
    }

    static String getSpotbugsToolVersion() {
        return getToolVersion(SPOTBUGS_ARTIFACT_NAME);
    }

    private static String getToolVersion(String toolArtifactName) {
        return requireNonNull(VERSION_PROPERTIES.getProperty(toolArtifactName));
    }
}
