package com.sixt.gradle.codequality;

import static java.util.stream.Stream.of;

import javax.annotation.Nonnull;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class CodeQualityPlugin implements Plugin<Project> {

    @Override
    public void apply(@Nonnull Project project) {
        of(
            new IdeaCodeStyleConfigurer(project),
            new CheckstyleConfigurer(project),
            new CircularDependencyCheckConfigurer(project),
            new SpotbugsConfigurer(project)
        ).forEach(Runnable::run);
    }

}
